using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;
using UnityEngine.Animations;
using UnityEngine.UI;

public class PlayerInputController : NetworkBehaviour
{

    // Serialized fields for the Rigidbody component of the player and the move speed and jump force values
    [SerializeField] private Rigidbody _characterRB;
    [SerializeField] public float moveSpeed, jumpForce, dodgeForce, acceleration, deceleration, dodgeTime;
    [SerializeField] private float energyDrain;

    [SerializeField] private Animator _animator;
    public FMODUnity.EventReference dashSoundEffect;

    private float currentSpeed;

    // Private Vector2 field to store the player's movement input
    private Vector2 _move;

    // Vector2 to store the mouse position
    private Vector2 _mousePos;

    // Timestamp for the last time the player jumped, dodged and the minimum interval for jumping and dodging
    private float _jumpTimeStamp = 0;
    private float _dodgeTimeStamp = 0;
    public float minJumpInterval, minDodgeInterval;

    // Bool to check if the player is grounded
    private bool _isGrounded;
    private bool isDodging = false;
    private Vector3 dodgeDirection;

    private float time;
    private float scaledValue;
    private Image dashIcon;
    private bool startTimer;
    private PlayerAbilitiesUI abilitiesUI;
    private Color32 fadedColor;
    private Color32 normalColor;
    private EnergyManagement energyManagement;

    void Awake()
    {
        _characterRB = GetComponent<Rigidbody>();
        energyManagement = GetComponent<EnergyManagement>();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        fadedColor = new Color32(255, 255, 255, 100);
        normalColor = new Color32(255, 255, 255, 255);
    }

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

        abilitiesUI = GetComponent<PlayerAbilitiesUI>();
        if (abilitiesUI.isSupportPlayer)
        {
            dashIcon = GameObject.Find("SupportDashIcon").GetComponent<Image>();

        }
        else
        {
            dashIcon = GameObject.Find("DamageDashIcon").GetComponent<Image>();
        }
        startTimer = false;
        var playerInput = GetComponent<PlayerInput>();
        playerInput.enabled = true;
    }

    private void Update()
    {


        if (startTimer)
        {

            time += Time.deltaTime;
            scaledValue = time / minDodgeInterval;
            dashIcon.fillAmount = scaledValue;

            if (time >= minDodgeInterval)
            {
                startTimer = false;
                dashIcon.fillAmount = 1;
                time = 0;
            }
        }

        if (isLocalPlayer)
        {
            if (energyManagement.CurrentEnergy < energyDrain)
            {
                dashIcon.color = fadedColor;
            }
            else
            {
                dashIcon.color = normalColor;
            }

        }

    }

    // Call the MovePlayer method in the Update method
    void FixedUpdate()
    {
        MovePlayer();
    }


    // Method to handle player movement input
    public void OnMove(InputAction.CallbackContext moveContext)
    {
        _move = moveContext.ReadValue<Vector2>();
    }

    // Method to update the player's position based on movement input
    public void MovePlayer()
    {
        if (!isLocalPlayer) return;
        if (isDodging)
        {
            if (_dodgeTimeStamp + dodgeTime <= Time.time)
            {
                isDodging = false;

            }
            else
            {
                _characterRB.velocity = dodgeDirection * dodgeForce;
                // _characterRB.MovePosition(transform.position + (dodgeDirection * dodgeForce * Time.deltaTime));
                // transform.Translate(dodgeDirection * dodgeForce * Time.fixedDeltaTime, Space.World);
                return;
            }
        }

        var right = Camera.main.transform.right;
        var forward = Vector3.Cross(right, Vector3.up);

        // Calculate the movement vector
        var movementRight = right * _move.x;
        var movementForward = forward * _move.y;

        var movement = movementRight + movementForward;

        // Convert the movement to a direction vector relative to the character look direction
        var localMovement = transform.InverseTransformDirection(movement).normalized;

        if (_animator != null)
        {
            _animator.SetFloat("X", localMovement.x, 0.15f, Time.fixedDeltaTime);
            _animator.SetFloat("Z", localMovement.z, 0.15f, Time.fixedDeltaTime);
        }

        // transform.Translate(movement * currentSpeed * Time.fixedDeltaTime, Space.World);
        _characterRB.velocity = movement * currentSpeed;

        if (movement != Vector3.zero)
        {
            currentSpeed = Mathf.Lerp(currentSpeed, moveSpeed, acceleration * Time.fixedDeltaTime);
        }
        else
        {
            currentSpeed = Mathf.Lerp(currentSpeed, 0, deceleration * Time.fixedDeltaTime);
        }

    }

    // This method is called whenever a "look" action is performed
    public void OnLook(InputAction.CallbackContext lookContext)
    {
        // Get the mouse position in screen coordinates
        Vector2 mousePosition = lookContext.ReadValue<Vector2>(); ;

        // Convert the mouse position to a ray that projects into the scene
        Ray mouseRay = Camera.main.ScreenPointToRay(mousePosition);

        // Define a plane that intersects with the scene at the object's position
        Plane groundPlane = new Plane(Vector3.up, transform.position);
        float rayDistance;

        // Check if the mouse ray intersects with the ground plane
        if (groundPlane.Raycast(mouseRay, out rayDistance))
        {
            // Get the point at which the mouse ray intersects with the ground plane
            Vector3 lookPoint = mouseRay.GetPoint(rayDistance);

            // Make the object look at the look point
            transform.LookAt(lookPoint);
        }
    }

    // Method to handle player jump input
    public void OnJump(InputAction.CallbackContext jumpContext)
    {
        // Check if the jump cooldown has ended
        bool jumpCooldownOver = (Time.time - _jumpTimeStamp) >= minJumpInterval;

        // If the jump cooldown has ended and the player is grounded, jump
        if (jumpCooldownOver && _isGrounded)
        {
            _jumpTimeStamp = Time.time;
            _characterRB.AddForce(Vector2.up * jumpForce, ForceMode.Impulse);
            _isGrounded = false;
        }
    }

    public void OnDodge(InputAction.CallbackContext dodgeContext)
    {
        // Check if the dodge cooldown has ended
        bool dodgeCooldownOver = (Time.time - _dodgeTimeStamp) >= minDodgeInterval;

        // If the dodge cooldown has ended and the player is grounded, dodge
        if (dodgeCooldownOver && energyManagement.CurrentEnergy >= energyDrain)
        {
            if (_move == Vector2.zero) return;


            if (_animator != null)
            {
                _animator.SetTrigger("Dash");
            }

            FMODUnity.RuntimeManager.PlayOneShot(dashSoundEffect, transform.position);

            startTimer = true;
            isDodging = true;

            CMDDrainEnergy();
            _dodgeTimeStamp = Time.time;

            var right = Camera.main.transform.right;
            var forward = Vector3.Cross(right, Vector3.up);

            // Calculate the movement vector
            var movementRight = right * _move.x;
            var movementForward = forward * _move.y;

            dodgeDirection = (movementForward + movementRight).normalized;
            // _characterRB.AddForce(dodgeDirection * dodgeForce, ForceMode.Impulse);
        }
    }

    [Command]
    private void CMDDrainEnergy()
    {

        energyManagement.ConsumeEnergy(energyDrain);


    }

    // Method to check if the player is touching the ground
    private void OnCollisionEnter(Collision col)
    {
        // If the player is touching a collider with the "Ground" tag, set _isGrounded to true
        if (col.gameObject.tag == "Ground")
        {
            _isGrounded = true;
        }
    }
}
