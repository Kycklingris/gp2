using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectInstance : MonoBehaviour
{
    [SerializeField] 
    private float timeUntilDestroy = 1.0f;

    void Start()
    {
        GetComponent<ParticleSystem>().Play();
        
        
    }

    private void LateUpdate()
    {
        if (!GetComponent<ParticleSystem>().isPlaying)
        {
            Destroy(this.gameObject, timeUntilDestroy);
        }
    }
}
