using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class HackingTerminal : NetworkBehaviour, IHackable
{
    private GameObject[] walls;

    void Start()
    {
        walls = GameObject.FindGameObjectsWithTag("ElectricWall");
        Debug.Log("Walls" + walls.Length);
    }

    void IHackable.StartedHack()
    {
        Debug.Log("Hacking walls");
        for(int i = 0; i < walls.Length; i++)
        {
            walls[i].GetComponent<ElectricWall>().DisableWall();
        }
    }

}
