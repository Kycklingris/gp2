using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Mirror;
using Unity.VisualScripting;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : NetworkBehaviour
{
    [SerializeField] private UnityEvent allPlayersFoundCallback;
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject gameOverScreen;

    public static GameManager gameManager;
    
    
    private GameObject[] _players;
    private bool _bossAlive = true;
    private bool _player1Alive = true;
    private bool _player2Alive = true;
    private bool _gameEnded = false;

    public GameObject[] players
    {
        get { return _players; }
    }

    private Coroutine searchForPlayersRoutine;


    private void Awake()
    {
        gameManager = this;
    }


    void Start()
    {
        searchForPlayersRoutine = StartCoroutine(SearchForPlayers());

    }

    public void AllPlayersReadyListener(UnityAction call)
    {
        if (_players != null && _players.Length == 2)
        {
            call.Invoke();
        }
        else
        {
            allPlayersFoundCallback.AddListener(call);
        }
    }

    IEnumerator SearchForPlayers()
    {
        while(true)
        {
            var foundPlayers = GameObject.FindGameObjectsWithTag("Player");

            if (foundPlayers.Length == 2)
            {
                _players = foundPlayers;
                allPlayersFoundCallback.Invoke();
                StopCoroutine(searchForPlayersRoutine);
            }

            yield return null;
        }
        
    }

    public void BossDeath(){
        _bossAlive = false;
    }

    public void PlayerDeath(GameObject player){
        if(player == _players[0]){
            _player1Alive = false;
        }
        else if(player == _players[1]){
            _player2Alive = false;
        }
    }

    void Update(){
        if(!_bossAlive && !_gameEnded){
            if (!isServer) return;
            Win();
        }

        if((!_player1Alive && !_player2Alive) && !_gameEnded){
            if (!isServer) return;
            Lose();
        }
    }


    public void PlayerHasDied()
    {
        Lose();
    }

    [ClientRpc]
    public void Win(){
            _gameEnded = true;
            _bossAlive = false;
            Debug.Log("You win!");
            winScreen.SetActive(true);
    }

    [ClientRpc]
    public void Lose(){
        _gameEnded = true;
        _bossAlive = true;
        Debug.Log("You lose!");
        gameOverScreen.SetActive(true);
        
    }

    [ClientRpc]
    public void RestartGame(){
        // Reset boss and player variables
        _bossAlive = true;
        _player1Alive = true;
        _player2Alive = true;
        _gameEnded = false;

        // Restart the level
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
    }
}
