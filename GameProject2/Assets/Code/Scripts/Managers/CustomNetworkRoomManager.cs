using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Unity.Mathematics;
using UnityEngine.SceneManagement;


public class CustomNetworkRoomManager : NetworkRoomManager
{
    [SerializeField] private NetworkIdentity damagePrefab;
    [SerializeField] private NetworkIdentity supportPrefab;

    [HideInInspector]
    public SelectedCharacter localPlayerCharacter = SelectedCharacter.none;

    List<NetworkConnectionToClient> players = new List<NetworkConnectionToClient>();

    public override void OnRoomStartServer()
    {
        base.OnRoomStartServer();
    }

    public override void OnRoomStartClient()
    {
        base.OnRoomStartClient();

        NetworkClient.RegisterPrefab(damagePrefab.gameObject);
        NetworkClient.RegisterPrefab(supportPrefab.gameObject);
    }

    public override GameObject OnRoomServerCreateGamePlayer(NetworkConnectionToClient conn, GameObject roomPlayer)
    {
        var selected = roomPlayer.GetComponent<LobbyPlayerScript>().selection;

        Destroy(roomPlayer);

        if (selected == SelectedCharacter.Attack) return Instantiate(damagePrefab.gameObject, new Vector3(-20, 0, -30), quaternion.identity);
        else return Instantiate(supportPrefab.gameObject, new Vector3(-30, 0, -20), quaternion.identity);

    }

    // public override void OnServerConnect(NetworkConnectionToClient conn)
    // {
    //     base.OnServerConnect(conn);

    //     players.Add(conn);
    // }

    // public override void OnServerDisconnect(NetworkConnectionToClient conn)
    // {

    //     if (SceneManager.GetActiveScene().name == "Game")
    //     {
    //         foreach (var client in players)
    //         {
    //             base.OnServerDisconnect(client);

    //         }

    //         StopHost();
    //         StopServer();
    //     }
    //     else
    //     {
    //         base.OnServerDisconnect(conn);
    //     }
    // }
}
