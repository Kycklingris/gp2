using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualEffectInstance : MonoBehaviour
{
    [SerializeField] 
    private float timeUntilDestroy = 2.0f;
    
    void Start()
    {
        Destroy(this.gameObject, timeUntilDestroy);
    }
}
