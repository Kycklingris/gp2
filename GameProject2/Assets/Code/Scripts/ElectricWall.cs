using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ElectricWall : NetworkBehaviour
{
    private GameObject wall;
    private bool isDisabled;
    private float counter;
    private float downTime;

    private void Start()
    {
        wall = transform.Find("Wall").gameObject;
        isDisabled = false;
        downTime = 5;
    }

    [ClientRpc]
    public void DisableWall()
    {
        Debug.Log("Disable wall");
        wall.SetActive(false);
        isDisabled = true;
        counter = 0;
    }

    private void EnableWall()
    {
        wall.SetActive(true);
        isDisabled = false;
    }

    private void DownTimer()
    {
        counter += Time.deltaTime;
        if (counter >= downTime)
        {
            EnableWall();
        }
    }


    private void Update()
    {

        if (isDisabled) { DownTimer(); }

    }


}
