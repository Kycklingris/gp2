using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Music : MonoBehaviour
{
    static private GameObject instance;
    void Start()
    {
        if (instance != null && instance != this.gameObject) 
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this.gameObject;
        DontDestroyOnLoad(this);

        SceneManager.activeSceneChanged += SceneChanged;
    }

    private void SceneChanged(Scene current, Scene next)
    {
        if (next.name != "MainMenuScene" && next.name != "Lobby")
        {
            instance = null;
            Destroy(this.gameObject);
        }
    }
}
