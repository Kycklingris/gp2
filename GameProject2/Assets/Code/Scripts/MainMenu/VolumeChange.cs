using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class VolumeChange : MonoBehaviour
{
    [SerializeField] private TMP_Text sfxVolume;
    [SerializeField] private TMP_Text musicVolume;


    public void SetSFX(float volume)
    {
        sfxVolume.text = ((int)(volume * 100)).ToString();
        FMODUnity.RuntimeManager.GetVCA("vca:/SFX").setVolume(volume);
    }

    public void SetMusic(float volume)
    {
        musicVolume.text = ((int)(volume * 100)).ToString();
        FMODUnity.RuntimeManager.GetVCA("vca:/Music").setVolume(volume);
    }
}
