using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonHighlight : MonoBehaviour
{
    // Generic fields
    [SerializeField] Button button;
    [SerializeField] TextMeshProUGUI buttonTextElement;
    [SerializeField] Image buttonIcon;
    [SerializeField] Color textHoverColor;

    // The different sprites for the button
    [SerializeField] Sprite[] buttonSprites;
    // 0 = Normal
    // 1 = Hover
    // 2 = Selected

    private Color originalTextColor;

    // Start is called before the first frame update
    void Start()
    {
        originalTextColor = buttonTextElement.color;
    }

    public void resetOnClick()
    {
        resetButton();
    }

    public void changeOnHover(bool hovering)
    {
        if (hovering)
        {
            buttonTextElement.color = textHoverColor;

            if (buttonIcon != null)
                buttonIcon.color = textHoverColor;

            button.image.sprite = buttonSprites[1];
        }
        else
        {
            resetButton();
        }
    }

    private void resetButton()
    {
        buttonTextElement.color = originalTextColor;

        if (buttonIcon != null)
            buttonIcon.color = originalTextColor;

        Debug.Log("ON MOUSE EXIT: Reverting button sprite to default!");
        button.image.sprite = buttonSprites[0];
    }
}
