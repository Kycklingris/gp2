using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
// using Mirror;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private GameObject _gameMenu;
    
    public void QuitGame()
    {
        var gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        foreach (var player in gameManager.players)
        {
            var playerExit = player.GetComponent<PlayerExit>();
            if (playerExit.isLocalPlayer)
            {
                playerExit.Exit();
            }
        }

        Application.Quit();
    }

    public void OpenGameMenu()
    {
        _gameMenu.SetActive(true);
    }

    public void ResumeGame()
    {
        _gameMenu.SetActive(false);
    }

    public void ReturnToMainMenu()
    {
        // GameObject.FindWithTag("NetworkManager").GetComponent<CustomNetworkRoomManager>().StopHost();
        var gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        foreach(var player in gameManager.players)
        {
            var playerExit = player.GetComponent<PlayerExit>();
            if (playerExit.isLocalPlayer)
            {
                playerExit.Exit();
            }
        }
    }
}