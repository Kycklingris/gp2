using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UICooldown : MonoBehaviour
{
    private Image thisImage;

    private float scaledValue;
    private float time;

    private void Awake()
    {
        thisImage = GetComponent<Image>();
    }


    void Start()
    {
        time = 0;
        scaledValue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        scaledValue = time / 20;
        thisImage.fillAmount = scaledValue;

        if (time >= 20)
        {
            time = 0;
        }
    }
}
