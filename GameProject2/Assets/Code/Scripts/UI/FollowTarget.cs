using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{

    private RectTransform rectTransform;
    private RectTransform canvasTransform;
    private Vector2 uiOffset;
    private Vector2 followOffset;
    private Vector2 proportionalPosition;


    private void Start()
    {
        canvasTransform = GameObject.FindGameObjectWithTag("Hud").GetComponent<RectTransform>();
        rectTransform = GetComponent<RectTransform>();
        uiOffset = new Vector2(canvasTransform.sizeDelta.x * .5f, canvasTransform.sizeDelta.y * .5f);
        followOffset = new Vector2(0, 40);
    }

    public void WorldSpaceToViewPort(GameObject target)
    {
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(target.transform.position);

        Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvasTransform.sizeDelta.x, ViewportPosition.y * canvasTransform.sizeDelta.y);

        rectTransform.localPosition = (proportionalPosition - uiOffset) + followOffset;
    }
}