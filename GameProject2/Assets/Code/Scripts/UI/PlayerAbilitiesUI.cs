using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAbilitiesUI : NetworkBehaviour
{
    public bool isSupportPlayer;

    private GameObject damageAbilitiesLayout;
    private GameObject supportAbilitiesLayout;

    private Image hackingIcon;
    private Image healingIcon;
    private Image dashIcon;
    private Image shootIcon;

    private TMP_Text localName;
    private TMP_Text onlineName;
    private string supportName;
    private string damageName;

    //private float hackingTime;
    //private float hackingcoolDown;
    //[NonSerialized] public bool hasHackingStarted; 

    EnergyManagement energyManagement;

    private void Awake()
    {
        damageAbilitiesLayout = GameObject.Find("DamageAbilities")/*.GetComponent<Image>()*/;
        supportAbilitiesLayout = GameObject.Find("SupportAbilities")/*.GetComponent<Image>()*/;

        //hackingIcon = supportAbilitiesLayout.transform.Find("HackingIcon").GetComponent<Image>();
        //healingIcon = supportAbilitiesLayout.transform.Find("HealingIcon").GetComponent<Image>();
        //dashIcon = supportAbilitiesLayout.transform.Find("DashIcon").GetComponent<Image>();
        //shootIcon = supportAbilitiesLayout.transform.Find("ShootIcon").GetComponent<Image>();

        localName = GameObject.Find("LocalPlayerName").GetComponent<TMP_Text>();
        onlineName = GameObject.Find("OnlinePlayerName").GetComponent<TMP_Text>();



    }
    void Start()
    {

    #region Setting up UI
        supportName = "Robot";
        damageName = "Human";

        if (isLocalPlayer)
        {

            if (isSupportPlayer)
            {
                damageAbilitiesLayout.SetActive(false);
                localName.text = supportName;

            }
            else
            {
                supportAbilitiesLayout.SetActive(false);
                localName.text = damageName;
            }
        }
        else
        {
            if (isSupportPlayer)
            {
                onlineName.text = supportName;
            }
            else
            {
                onlineName.text = damageName;
            }
        }
    #endregion


    }


    void Update()
    {
        //if (isLocalPlayer)
        //{
        //    if (isSupportPlayer)
        //    {

        //    }
        //}
    }
}
