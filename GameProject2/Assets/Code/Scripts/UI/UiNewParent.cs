using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiNewParent : MonoBehaviour
{

    private Canvas canvas;

    private void Start()
    {
        canvas = GameObject.Find("PlayerHud").GetComponent<Canvas>();
        this.gameObject.transform.SetParent(canvas.transform);
    }



}
