using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSystemHealth : NetworkBehaviour, IHackable
{
    public float health;
    [SerializeField] GameObject boss;

    public FMODUnity.EventReference systemHurtSound;
    public FMODUnity.EventReference systemDownSound;

    private ResourceSystem healthSystem;
    private BossHealthScript bossHealth;

    private bool dead = false;

    private void Awake()
    {
        //health = 500;

        healthSystem = new ResourceSystem(health);
        bossHealth = boss.GetComponent<BossHealthScript>();

    }

    private void SystemTakeDamage(float damage)
    {
        health = healthSystem.ChangeValue(damage);
        bossHealth.BossHealth();
        Debug.Log("AUGH " + bossHealth.health);
        if (isServer)
        {
            PlayAudio(systemHurtSound);
        }

        if (health == 0)
        {
            if (isServer)
            {
                if (!dead)
                {
                    PlayAudio(systemDownSound);
                }
                dead = true;
            }
            bossHealth.systems.Remove(this.gameObject);
            Destroy(gameObject);

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet") && isServer)
        {

            BulletController bulletController = collision.gameObject.GetComponent<BulletController>();
            SystemTakeDamage(-bulletController.damage);
            //Debug.Log("AUGH " + bossHealth.health);
            if (health == 0)
            {
                bossHealth.systems.Remove(this.gameObject);
                Destroy(gameObject);

            }
        }

    }

    public void StartedHack()
    {
        Debug.Log("Hacking System");
        SystemTakeDamage(-100);
    }

    [ClientRpc]
    private void PlayAudio(FMODUnity.EventReference audio)
    {
        FMODUnity.RuntimeManager.PlayOneShot(audio, transform.position);
    }
}
