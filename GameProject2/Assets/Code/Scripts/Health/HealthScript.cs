using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using Unity.VisualScripting;
using System;
using UnityEditor;

public class HealthScript : NetworkBehaviour, IThrowableAction
{
    //A ScriptableObject holding data

    public FMODUnity.EventReference PlayerDeathEvent;
    public FMODUnity.EventReference PlayerHurtEvent;

    [SerializeField] private Stats stats;

    [SyncVar]
    [NonSerialized] public float health;

    private ReviveScript reviveScript;
    private HealthBar healthBar;
    private PlayerInputController playerInputController;
    public ResourceSystem healthSystem;

    private void Awake()
    {
        healthBar = GetComponent<HealthBar>();
        reviveScript = GetComponent<ReviveScript>();
        playerInputController = GetComponent<PlayerInputController>();
        stats.SetUp();

    }

    private void Start()
    {
        healthSystem = new ResourceSystem(stats.maxHealth);
        health = healthSystem.Amount;
        healthBar.SetHealthValue(health, 0.75f);


    }


    private void FixedUpdate()
    {

        if (!isLocalPlayer) return;
        if (stats.enableHealthRegeneration && !reviveScript.isPlayerDowned)
        {
            CMDChangedHealth(stats.healthRegeneration * Time.fixedDeltaTime);
            //Debug.Log(stats.currentHealth);

        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("BossAttack"))
        {

            if (isLocalPlayer)
            {
                RPCOnHurt();
                CMDChangedHealth(-10);
            }

        }

    }

    [ClientRpc]
    private void RPCUpdateBars()
    {
        healthBar.UpdateHealthValue(health, 0.75f);

    }

    [ClientRpc]
    private void RPCOnDeath()
    {
        FMODUnity.RuntimeManager.PlayOneShot(PlayerDeathEvent, transform.position);
    }

    [ClientRpc]
    private void RPCOnHurt()
    {
        FMODUnity.RuntimeManager.PlayOneShot(PlayerHurtEvent, transform.position);
    }

    [Command]
    public void CMDChangedHealth(float value)
    {
        this.health = healthSystem.ChangeValue(value);
        if (this.health == 0)
        {

            if (reviveScript.isPlayerDowned == false)
            {
                RPCOnDeath();
            }
            reviveScript.isPlayerDowned = true;
            stats.healthState = Stats.HealthState.Downed;
        }
        RPCUpdateBars();
    }



    [ClientRpc]
    private void RPCTrowAction(ThrowableAction action, float value)
    {
        if (!isLocalPlayer) return;
        if (!reviveScript.isPlayerDowned)
        {
            CMDChangedHealth(value);

        }
    }

    
    public void ThrowAction(ThrowableAction action, float value)
    {


        RPCTrowAction(action, value);
    }
}
