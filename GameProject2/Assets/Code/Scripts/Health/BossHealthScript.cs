using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using System;

public class BossHealthScript : NetworkBehaviour
{
    [SyncVar(hook = nameof(UpdateHealthBar))]
    [NonSerialized] public float health;
    [SerializeField] private Slider healthSlider;

    [SerializeField] public List<GameObject> systems;


    private void Awake()
    {
        BossHealth();
        //Debug.Log(health);
        
        //healthSlider = GameObject.Find("BossHealthBar").GetComponent<Slider>();
        SetHealthBar();

    }

    public void BossHealth()
    {
        health = 0;
        foreach (var system in systems)
        {
            BossSystemHealth systemHealth = system.GetComponent<BossSystemHealth>();
            health += systemHealth.health;
        }
        if (health == 0)
        {
            //Replace with game over code
            GameManager.gameManager.Win();
            Destroy(gameObject);
        }
    }

    private void SetHealthBar()
    {
        healthSlider.maxValue = health;
        healthSlider.value = health;

    }


    private void UpdateHealthBar(float _Old, float _New)
    {

        healthSlider.value = health;

    }




}
