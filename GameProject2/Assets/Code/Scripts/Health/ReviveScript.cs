using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using UnityEngine.InputSystem;
using System.Net.Http.Headers;
using System.Text;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;
using DG.Tweening.Core.Easing;
using Newtonsoft.Json.Linq;
using UnityEngine.Rendering;
using Unity.VisualScripting;
using TMPro;

public class ReviveScript : NetworkBehaviour
{
    GameManager gameManager;
    private HealthScript healthScript;
    private PlayerInputController playerInput;

    private Canvas playerCanvas;
    private RectTransform canvasRectTranform;
    private Vector2 uiOffset;

    private GameObject secondPlayer;

    private GameObject localReviveIcon;
    private GameObject reviveIcon;
    private GameObject reviveIndicator;
    private GameObject localReviveIndicator;
    //private TMP_Text localReviveText;
    //private TMP_Text reviveText;

    private Image localReviveBorder;
    private Image reviveBorder;
    private Image indicatorBorder;
    private Image localIndicatorBorder;

    [SerializeField] private GameObject reviveVisualization;

    [Header("Revive Settings")]
    //BOOLS
    #region Bools
    [SyncVar(hook = nameof(RPCSetPlayerDownedBool))]
    [NonSerialized] public bool isPlayerDowned;
    private bool isPlayerBeingRevived;
    private bool isPlayerCloseEnough;
    private bool test;
    #endregion

    //FLOATS
    #region Floats
    [SerializeField] private float downedTime;

    //[SyncVar(hook = nameof(SetReviveText))]
    private float countDown;
    [SyncVar(hook = nameof(SetScaledValue))]
    private float scaledValue;
    private float countUp;
    private float reviveTime;
    private float reviveRadius;
    private float originalMovementSpeed;
    private float originalDashForce;
    private float originalDodgeTime;


    #endregion

    //VECTORS
    #region Vectors
    private Vector3 offset;
    private Vector3 reviveVisualizationSize;
    private Vector3 reviveVisualizationLocation;
    [SerializeField] private Vector3 secondPlayerPosition;
    #endregion

    private void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.AllPlayersReadyListener(FindPlayer);

        healthScript = GetComponent<HealthScript>();
        playerInput = GetComponent<PlayerInputController>();

        #region CURSED HUD
        const string hudTag = "Hud";
        playerCanvas = GameObject.FindGameObjectWithTag(hudTag).GetComponent<Canvas>();

        reviveIcon = playerCanvas.transform.Find("OnlineReviveProgression").gameObject;
        reviveBorder = reviveIcon.transform.Find("Border").GetComponent<Image>();
        reviveIndicator = playerCanvas.transform.Find("OnlineReviveIndicator").gameObject;
        indicatorBorder = reviveIcon.transform.Find("Border").GetComponent<Image>();
        //reviveText = reviveIcon.transform.Find("OnlineDownedTimeText").GetComponent<TMP_Text>();


        localReviveIcon = playerCanvas.transform.Find("LocalReviveProgression").gameObject;
        localReviveBorder = localReviveIcon.transform.Find("Border").GetComponent<Image>();
        localReviveIndicator = playerCanvas.transform.Find("LocalReviveIndicator").gameObject;
        localIndicatorBorder = localReviveIcon.transform.Find("Border").GetComponent<Image>();
        //localReviveText = localReviveIcon.transform.Find("LocalDownedTimeText").GetComponent<TMP_Text>();

        reviveIcon.SetActive(false);
        localReviveIcon.SetActive(false);
        localReviveIndicator.SetActive(false);
        reviveIndicator.SetActive(false);
        #endregion

    }

    private void Start()
    {

        canvasRectTranform = playerCanvas.GetComponent<RectTransform>();

        #region Setting Values
        //REVIVE BOOLS
        isPlayerDowned = false;
        isPlayerBeingRevived = false;
        isPlayerCloseEnough = false;
        test = false;

        //REVIVE TIMER VARIABLES
        //downedTime = 25;
        countDown = downedTime;
        countUp = 0;
        reviveTime = 5;

        //REVIVE RANGE VARIABLES
        #region RANGE VARIABLES
        reviveRadius = 5;
        reviveVisualizationSize = new Vector3(reviveRadius * 2, 0.3f, reviveRadius * 2);
        reviveVisualizationLocation = new Vector3(0, 0, 0);
        reviveVisualization.transform.localScale = reviveVisualizationSize;
        reviveVisualization.transform.localPosition = reviveVisualizationLocation;
        #endregion

        reviveVisualization.SetActive(false);
        offset = new Vector3(0, 3, 0);

        originalMovementSpeed = playerInput.moveSpeed;
        originalDashForce = playerInput.dodgeForce;
        originalDodgeTime = playerInput.dodgeTime;

        #endregion


    }

    private void Update()
    {
        if (secondPlayer != null)
        {
            if (isPlayerDowned && Vector3.Distance(this.gameObject.transform.position, secondPlayer.transform.position) < reviveRadius)
            {
                isPlayerBeingRevived = true;
            }
            else
            {
                isPlayerBeingRevived = false;
                countUp = 0;
            }

        }

        if (isPlayerDowned && !isPlayerBeingRevived)
        {
            ReviveCountdown();
        }

        if (isPlayerDowned && isPlayerBeingRevived)
        {

            RevivePlayer();

        }

    }

    private void FixedUpdate()
    {
        FollowTarget followTarget;

        if (test)
        {

            if (!isLocalPlayer)
            {

                followTarget = reviveIcon.GetComponent<FollowTarget>();
                followTarget.WorldSpaceToViewPort(this.gameObject);
                followTarget = reviveIndicator.GetComponent<FollowTarget>();
                followTarget.WorldSpaceToViewPort(this.gameObject);
                //reviveIcon.transform.position = secondPlayer.transform.position + offset;
            }
        }
    }

    private void ReviveCountdown()
    {
        if (isLocalPlayer)
        {
            localReviveIndicator.SetActive(true);
            localReviveIcon.SetActive(false);
        }
        else
        {
            reviveIndicator.SetActive(true);
            reviveIcon.SetActive(false);
        }

        countDown -= Time.deltaTime;
        scaledValue = countDown / downedTime;

        if (countDown <= 0)
        {
            //Replace with kill code
            //gameManager.players.dele
            gameManager.Lose();
            Destroy(gameObject);
        }

    }

    public void RevivePlayer()
    {
        if (isLocalPlayer)
        {
            localReviveIndicator.SetActive(false);
            localReviveIcon.SetActive(true);
        }
        else
        {
            reviveIndicator.SetActive(false);
            reviveIcon.SetActive(true);
        }
        countUp += Time.deltaTime;
        scaledValue = countUp / reviveTime;

        if (countUp >= reviveTime)
        {
            isPlayerDowned = false;
            isPlayerBeingRevived = false;
            healthScript.health = healthScript.healthSystem.GainResource(healthScript.healthSystem.MaxAmount / 5);
            healthScript.CMDChangedHealth(1);
            countDown = downedTime;
            countUp = 0;
            //Debug.Log("player Healed");
        }

    }




    private void SetScaledValue(float oldValue, float newValue)
    {
        scaledValue = newValue;

        if (isLocalPlayer)
        {
            localReviveBorder.fillAmount = scaledValue;
        }
        if (!isLocalPlayer)
        {
            reviveBorder.fillAmount = scaledValue;

        }
    }

    //private void SetReviveText(float _Old, float _New)
    //{
    //    countDown = _New;

    //    if (isLocalPlayer)
    //    {
    //        localReviveText.text = countDown.ToString();
    //    }
    //    if (!isLocalPlayer)
    //    {
    //        reviveText.text = countDown.ToString();
    //    }
    //}


    [ClientRpc] //Runs on all clients
    private void RPCSetPlayerDownedBool(bool oldBool, bool newBool)
    {
        isPlayerDowned = newBool;
        FollowTarget followTarget;

        test = isPlayerDowned;

        if (isLocalPlayer)
        {
            localReviveIcon.SetActive(isPlayerDowned);
            localReviveIndicator.SetActive(isPlayerDowned);

            followTarget = localReviveIcon.GetComponent<FollowTarget>();
            followTarget.WorldSpaceToViewPort(this.gameObject);
            followTarget = localReviveIndicator.GetComponent<FollowTarget>();
            followTarget.WorldSpaceToViewPort(this.gameObject);

            //    //localReviveIcon.transform.position = this.gameObject.transform.position + offset;
        }
        if (!isLocalPlayer)
        {
            reviveIndicator.SetActive(isPlayerDowned);
            reviveIcon.SetActive(isPlayerDowned);

            //    followTarget = reviveIcon.GetComponent<FollowTarget>();
            //    followTarget.WorldSpaceToViewPort(this.gameObject);
            //    followTarget = reviveIndicator.GetComponent<FollowTarget>();
            //    followTarget.WorldSpaceToViewPort(this.gameObject);
            //    //reviveIcon.transform.position = secondPlayer.transform.position + offset;
        }

        if (isPlayerDowned)
        {
            playerInput.dodgeForce = 0;
            playerInput.dodgeTime = 0;
            playerInput.moveSpeed = 0;
        }
        else
        {
            playerInput.dodgeForce = originalDashForce;
            playerInput.dodgeTime = originalDodgeTime;
            playerInput.moveSpeed = originalMovementSpeed;
        }
        reviveVisualization.SetActive(isPlayerDowned);

    }



    private void FindPlayer()
    {

        foreach (GameObject player in gameManager.players)
        {
            if (this.gameObject != player)
            {
                secondPlayer = player;
            }
        }
    }

    [Command]
    private void CMDRevivePlayer()
    {

    }
}
