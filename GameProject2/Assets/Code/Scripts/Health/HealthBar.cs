using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
//using UnityEngine.UIElements;

public class HealthBar : NetworkBehaviour
{

    private Slider healthBar;
    private Slider localHealthBar;
    private Slider localEnergyBar;
    private Slider energyBar;
   

    private void Awake()
    {
        const string hudTag = "Hud";
        var Hud = GameObject.FindGameObjectWithTag(hudTag);

        GameObject localPlayerProfile;
        GameObject playerProfile;
        

        localPlayerProfile = Hud.transform.Find("LocalPlayerPortrait").gameObject;

        playerProfile = Hud.transform.Find("SecondPlayerPortrait").gameObject;


        localHealthBar = localPlayerProfile.transform.Find("HealthBarSlider").GetComponent<Slider>();
        healthBar = playerProfile.transform.Find("HealthBarSlider").GetComponent<Slider>();

        localEnergyBar = localPlayerProfile.transform.Find("EnergyBarSlider").GetComponent<Slider>();
        energyBar = playerProfile.transform.Find("EnergyBarSlider").GetComponent<Slider>();


        //Debug.Log(this.gameObject.name + healthBar);
    }


    public void SetHealthValue(float maxValue, float scale)
    {
        if (isLocalPlayer)
        {
            localHealthBar.maxValue = maxValue;
            localHealthBar.value = maxValue * scale;

        }
        else if (!isLocalPlayer)
        {
            healthBar.maxValue = maxValue;
            healthBar.value = maxValue * scale;

        }


    }

    public void UpdateHealthValue(float currentHealth, float scale)
    {
        if (isLocalPlayer)
        {
            localHealthBar.value = currentHealth * scale;

        }
        else if (!isLocalPlayer)
        {
            healthBar.value = currentHealth * scale;

        }

    }

    public void SetEnergyValue(float maxValue, float scale)
    {
        if (isLocalPlayer)
        {
            localEnergyBar.maxValue = maxValue;
            localEnergyBar.value = maxValue * scale;

        }
        else if (!isLocalPlayer)
        {
            energyBar.maxValue = maxValue;
            energyBar.value = maxValue * scale;

        }
    }

    public void UpdateEnergyValue(float currentValue, float scale)
    {
        if (isLocalPlayer)
        {
            localEnergyBar.value = currentValue * scale;

        }
        else if (!isLocalPlayer)
        {
            energyBar.value = currentValue * scale;

        }

    }
}
