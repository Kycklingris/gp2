using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Kept in this script to make sure that only the hacking sphere collider is used.
public class HackingCollider : MonoBehaviour
{
    [SerializeField] private Hacking hackingScript;

    private void Awake()
    {
        hackingScript = transform.parent.GetComponent<Hacking>();
    }

    private void OnTriggerEnter(Collider other)
    {
        IHackable target = other.GetComponentInParent<IHackable>();
        if (target != null)
        {
            hackingScript.TargetEntered(target);
        }
    }

}
