using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;
using UnityEngine.UI;

public class Hacking : NetworkBehaviour
{
    [SerializeField] private GameObject hackingField;
    [SerializeField] private HealthScript healthScript;
    // [SerializeField] private Stats stats;
    [SerializeField] private float energyDrain;
    [SerializeField] private float activeTime;
    [SerializeField] private float activeMoveSpeed;
    [SerializeField] private PlayerInputController playerInputController;
    [SerializeField] private Animator animator;
    public FMODUnity.EventReference hackingSoundEffect;
    public EnergyManagement energyManager;

    private Color32 fadedColor;
    private Color32 normalColor;

    private bool hacking = false;
    private float originalMoveSpeed;

    private float time;
    private float scaledValue;
    private Image hackingIcon;
    private bool startTimer;


    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        originalMoveSpeed = playerInputController.moveSpeed;
        hackingIcon = GameObject.Find("HackingIcon").GetComponent<Image>();
        fadedColor = new Color32(255, 255, 255, 100);
        normalColor = new Color32(255, 255, 255, 255);

    }

    private void Start()
    {
        startTimer = false;

        
    }

    private void Update()
    {
        if (startTimer)
        {
            time += Time.deltaTime;
            scaledValue = time / activeTime;
            hackingIcon.fillAmount = scaledValue;

            if (time >= activeTime)
            {
                startTimer = false;
                hackingIcon.fillAmount = 1;
                time = 0;
            }
        }


        if (isLocalPlayer)
        {
            if (energyManager.CurrentEnergy < energyDrain)
            {
                hackingIcon.color = fadedColor;
            }
            else
            {
                hackingIcon.color = normalColor;
            }

        }
    }

    public void OnHackClick(InputAction.CallbackContext context)
    {
        if (!context.performed) return;
        if (hacking) return;

        if (context.ReadValue<float>() > 0.5f && energyManager.CurrentEnergy >= energyDrain)
        { // Pressed
            // Hacking started
            startTimer = true;
            CMDStartHacking();
        }
    }

    private void DrainEnergy()
    {
        // Drain Energy
        energyManager.ConsumeEnergy(energyDrain);
        // healthScript.
    }

    [Command]
    private void CMDStartHacking()
    {
        RPCStartHacking();
        DrainEnergy();
        StartCoroutine(ActiveTime());
    }

    [ClientRpc]
    private void RPCStartHacking()
    {
        hacking = true;
        hackingField.SetActive(true);

        animator.SetTrigger("Hacking");
        FMODUnity.RuntimeManager.PlayOneShot(hackingSoundEffect, transform.position);

        if (isLocalPlayer)
        {
            playerInputController.moveSpeed = activeMoveSpeed;
        }
    }

    [ClientRpc]
    private void RPCStopHacking()
    {
        hacking = false;

        hackingField.SetActive(false);

        if (isLocalPlayer)
        {
            playerInputController.moveSpeed = originalMoveSpeed;
        }
    }

    IEnumerator ActiveTime()
    {
        yield return new WaitForSeconds(activeTime);

        // Hacking disabled
        RPCStopHacking();
        hacking = false;
        hackingField.SetActive(false);

    }

    public void TargetEntered(IHackable target)
    {
        if (!hacking || !isServer) return;

        target.StartedHack();
    }
}
