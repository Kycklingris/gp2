using Mirror;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class EnergyManagement : NetworkBehaviour
{
    public float maxEnergy = 100f;
    public float rechargeSpeed = 10f;

    [SyncVar(hook = nameof(UpdateEnergyBar))]
    private float currentEnergy;
    public Slider energySlider;

    public float CurrentEnergy
    {
        get { return currentEnergy; }

    }

    private HealthBar energyBar;

    private void Awake()
    {
        energyBar = GetComponent<HealthBar>();
    }

    // Start is called before the first frame update
    void Start()
    {
        currentEnergy = maxEnergy;
        energyBar.SetEnergyValue(maxEnergy, 0.25f);
    }

    // Update is called once per frame
    void Update()
    {
        Recharge();

        //energyBar.UpdateEnergyValue(currentEnergy, 0.25f);
        //if (energySlider != null)
        //{
        //    energySlider.value = currentEnergy;

        //}       
    }

    private void Recharge(){
        if(currentEnergy < maxEnergy){
            currentEnergy += rechargeSpeed * Time.deltaTime;
            currentEnergy = Mathf.Clamp(currentEnergy, 0f, maxEnergy);
        }
    }

    public bool ConsumeEnergy(float amount){
        if(amount <= currentEnergy){
            currentEnergy -= amount;
            return true;
        }
        else{
            return false;
        }
    }

    private void UpdateEnergyBar(float _Old, float _New)
    {
        energyBar.UpdateEnergyValue(currentEnergy, 0.25f);
    }

    public float GetMaxEnergy(){
        return maxEnergy;
    }

    public float GetCurrentEnergy(){
        return currentEnergy;
    }
}
