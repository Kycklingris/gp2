using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.UI;

public class SupportFire : NetworkBehaviour
{
    [SerializeField] private Transform firePoint;
    [SerializeField] private BulletController bulletPrefab;
    [SerializeField] private float fireRate;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private float bulletEnergyCost;
    [SerializeField] private int damage;

    public FMODUnity.EventReference playerShootSoundEffect;
    [SerializeField] private Animator animator;
    [SerializeField] private EnergyManagement energyManager;

    private float lastShot;

    private Coroutine rapidFire;

    private float time;
    private float scaledValue;
    private Image shootIcon;
    private bool startTimer;
    private Color32 fadedColor;
    private Color32 normalColor;

    private void Awake()
    {
        lastShot = -fireRate;
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        startTimer = false;
        time = 0;
        shootIcon = GameObject.Find("SupportShootIcon").GetComponent<Image>();
        fadedColor = new Color32(255, 255, 255, 100);
        normalColor = new Color32(255, 255, 255, 255);
    }

    private void Start()
    {
    }

    private void Update()
    {
        if (startTimer)
        {
           // Debug.Log(time);
            time += Time.deltaTime;
            scaledValue = time / fireRate;
            shootIcon.fillAmount = scaledValue;

            if (time >= fireRate)
            {
                startTimer = false;
                shootIcon.fillAmount = 1;
                time = 0;
            }
        }

        if (isLocalPlayer)
        {
            if (energyManager.CurrentEnergy < bulletEnergyCost)
            {
                shootIcon.color = fadedColor;
            }
            else
            {
                shootIcon.color = normalColor;
            }

        }
    }

    public void OnFire(InputAction.CallbackContext context)
    {
        if (context.performed && energyManager.CurrentEnergy >= bulletEnergyCost)
        {


            if (context.interaction is HoldInteraction )
            {
                animator.SetBool("Shooting", true);
                animator.SetTrigger("Shoot");
                rapidFire = StartCoroutine(RapidFire());
                
            }
            else
            {
                animator.SetBool("Shooting", false);
                animator.SetTrigger("Shoot");
                if (lastShot + fireRate >= Time.time)
                {
                    return;
                }

                lastShot = Time.time;
                CMDShoot();
                startTimer = true;
            }
        }

        if(context.canceled)
        {
            if (rapidFire != null)
            {
                StopCoroutine(rapidFire);
            }
            animator.SetBool("Shooting", false);
        }
    }

    private IEnumerator RapidFire() 
    {
        while(true)
        {
            CMDShoot();
            startTimer = true;
            yield return new WaitForSeconds(fireRate);
        }
    }

    [Command]
    private void CMDShoot()
    {
        if (energyManager.ConsumeEnergy(bulletEnergyCost))
        {
            GameObject bullet = Instantiate(bulletPrefab.gameObject, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bulletSpeed, ForceMode.Impulse);

            BulletController bc = bullet.GetComponent<BulletController>();

            RPCShoot();
            NetworkServer.Spawn(bullet);
        }
    }

    [ClientRpc]
    private void RPCShoot()
    {
        FMODUnity.RuntimeManager.PlayOneShot(playerShootSoundEffect, transform.position);
    }
}
