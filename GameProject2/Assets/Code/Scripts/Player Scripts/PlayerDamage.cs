using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using Mirror;
using UnityEngine.UI;
using System;

public class PlayerDamage : NetworkBehaviour
{

    [Header("GameObjects, transforms etc")]
    public GameObject bulletPrefab;
    public GameObject bigBulletPrefab;
    public Transform firePoint;
    [SerializeField] private Animator animator;
    private EnergyManagement energyManager;

    [Header("Primary fire settings")]
    public float fireRate;
    public float bulletSpeed;
    public int damage;
    public float bulletEnergyCost;
    private float lastFireTimeStamp;

    Coroutine fireCoroutine;
    WaitForSeconds rapidFireWait;

    [Header("Secondary fire settings")]
    [SerializeField] private int chargeTime;
    public int chargedDamage;
    public float bigBulletEnergyCost;
    public float bigBulletSpeed;
    private bool isCharging = false;
    private float chargeStartTime;

    public FMODUnity.EventReference playerShootSoundEffect;

    private float time;
    private float scaledValue;
    private Image shootIcon;
    private bool startTimer;
    private Color32 fadedColor;
    private Color32 normalColor;

    private float secondaryTime;
    private float secondaryScaledValue;
    private Image secondaryShootIcon;
    private bool secondaryStartTimer;


    private void Awake()
    {
        rapidFireWait = new WaitForSeconds(1 / fireRate);
        lastFireTimeStamp = -(1 / fireRate);
    }

    void Start()
    {
        //bigBulletEnergyCost = 5;
        energyManager = GetComponent<EnergyManagement>();
        startTimer = false;
        time = 0;
        secondaryTime = 0;
        secondaryStartTimer = false;
        shootIcon = GameObject.Find("DamageShootIcon").GetComponent<Image>();
        secondaryShootIcon = GameObject.Find("DamageChargedShot").GetComponent<Image>();
        fadedColor = new Color32(255, 255, 255, 100);
        normalColor = new Color32(255, 255, 255, 255);

    }

    private void Update()
    {

        if (startTimer)
        {
           // Debug.Log(time);
            time += Time.deltaTime;
            scaledValue = time / fireRate;
            shootIcon.fillAmount = scaledValue;

            if (time >= fireRate)
            {
                startTimer = false;
                shootIcon.fillAmount = 1;
                time = 0;
            }
        }

        if (isCharging && energyManager.CurrentEnergy >= bigBulletEnergyCost)
        {
            secondaryTime += Time.deltaTime;
            secondaryScaledValue = secondaryTime / chargeTime;
            secondaryShootIcon.fillAmount = secondaryScaledValue;
        }

        if (isLocalPlayer)
        {
            if (energyManager.CurrentEnergy < bulletEnergyCost)
            {
                shootIcon.color = fadedColor;
            }
            else
            {
                shootIcon.color = normalColor;
            }

            if (energyManager.CurrentEnergy < bigBulletEnergyCost)
            {
                secondaryShootIcon.color = fadedColor;
            }
            else
            {
                secondaryShootIcon.color = normalColor;
            }

        }
    }

    public void OnFire(InputAction.CallbackContext shootContext)
    {
        if (shootContext.performed && energyManager.CurrentEnergy >= bulletEnergyCost)
        {
            if (shootContext.interaction is HoldInteraction)
            {
                animator.SetBool("Shooting", true);
                animator.SetTrigger("Shoot");
                StartFiring();
            }
            else
            {
                animator.SetBool("Shooting", false);
                animator.SetTrigger("Shoot");
                if (lastFireTimeStamp + (1 / fireRate) < Time.time)
                {
                    Shoot();
                }

            }
        }

        if (shootContext.canceled)
        {
            animator.SetBool("Shooting", false);
            StopFiring();
        }
    }

    public void OnSecondaryFire(InputAction.CallbackContext chargeContext)
    {

        if (chargeContext.started)
        {
            isCharging = true;
            chargeStartTime = Time.time;
        }
        if (chargeContext.canceled)
        {
            float chargeDuration = Time.time - chargeStartTime;
            if (chargeDuration < chargeTime)
            {
                // animator.SetTrigger("Shoot");
                // Shoot();
            }
            else
            {
                animator.SetTrigger("Shoot");
                CMDSecondFire();
            }
            isCharging = false;
            secondaryTime = 0;
        }
    }

    void Shoot()
    {
        lastFireTimeStamp = Time.time;
        CMDShoot();
        startTimer = true;
        return;
    }

    [Command]
    void CMDShoot()
    {
        if (energyManager.ConsumeEnergy(bulletEnergyCost))
        {

            // The player has enough energy, shoot the bullet.
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bulletSpeed, ForceMode.Impulse);
            BulletController bc = bullet.GetComponent<BulletController>();
            RPCShoot();
            NetworkServer.Spawn(bullet);
        }
    }

    [ClientRpc]
    private void RPCShoot()
    {
        FMODUnity.RuntimeManager.PlayOneShot(playerShootSoundEffect, transform.position);
    }


    [Command]
    void CMDSecondFire()
    {
        if (energyManager.ConsumeEnergy(bigBulletEnergyCost))
        {
            // The player has enough energy, shoot the big bullet.
            GameObject bigBullet = Instantiate(bigBulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bigBullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bigBulletSpeed, ForceMode.Impulse);
            BulletController bc = bigBullet.GetComponent<BulletController>();
            NetworkServer.Spawn(bigBullet);
        }
    }

    public IEnumerator RapidFire()
    {
        float nextFire = 0f;

        while (true)
        {
            if (Time.time >= nextFire)
            {
                CMDShoot();
                startTimer = true;
                nextFire = Time.time + 1 / fireRate;
                yield return rapidFireWait;
            }
        }
    }

    void StartFiring()
    {
        fireCoroutine = StartCoroutine(RapidFire());
    }


    void StopFiring()
    {
        if (fireCoroutine != null)
        {
            StopCoroutine(fireCoroutine);
        }
    }
}
