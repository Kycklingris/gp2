using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerExit : NetworkBehaviour
{
    public void Exit()
    {
        CMDExit();
    }

    [Command]
    private void CMDExit()
    {
        RPCExit();
        NetworkManager.singleton.StopHost();
    }

    [ClientRpc]
    private void RPCExit()
    {
        NetworkManager.singleton.StopHost();
    }
}
