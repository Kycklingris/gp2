using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ProjectileInstance : NetworkBehaviour
{
    private Rigidbody rb;
    [SerializeField]
    public Projectile projectile;
    
    private Vector3 LookPos;
    [SerializeField] 
    private float timeUntilDestroy = 1.0f;
    
    void Start()
    {
        if (!isServer) return;
		Destroy(this.gameObject, timeUntilDestroy);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            Instantiate(projectile.OnDeath, transform.position, transform.rotation);
            //TODO: put AOE here
            Destroy(this.gameObject);
        }
        
        if (collision.gameObject.CompareTag("Player"))
        {
            Instantiate(projectile.OnDeath, transform.position, transform.rotation);
            //TODO: put AOE here
            Destroy(this.gameObject);
        }
    }

    private void LateUpdate()
    {
		if (!isServer) return;

		rb ??= gameObject.GetComponent<Rigidbody>();
        
        var LookPos = rb.velocity.normalized;
        transform.LookAt(transform.position + LookPos);
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
    }
}
