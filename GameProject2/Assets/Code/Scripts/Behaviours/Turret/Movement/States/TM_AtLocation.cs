using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TM_AtLocation : ITurretMoveState
{
    public virtual ITurretMoveState DoState(TurretMoveFSM obj)
    {
        obj.stateIndicator = TurretMoveFSMStates.AtLocation;
        // If we have arrived then break loop
        if (obj.currentNode == obj.targetNode)
        {
            obj.HasReachedTarget = true;
            obj.direction = TurretMoveDirection.none;
            obj.targetNode = null;
            obj.targetDirection = TurretMoveDirection.none;
            return obj.AtLocation;
        }
        obj.HasReachedTarget = false;

        // State

        
        //nothing
        
        
        
        // State Exit

        if (obj.currentNode.Sector == obj.targetSector)
            return obj.AtLocation;

        return obj.SearchTarget;
    }
}
