using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TM_SearchTarget : ITurretMoveState
{
    public virtual ITurretMoveState DoState(TurretMoveFSM obj)
    {
        obj.stateIndicator = TurretMoveFSMStates.SearchTarget;
        //exit if we are at correct node
        if (obj.currentNode == obj.targetNode)
        {
            obj.direction = TurretMoveDirection.none;
            obj.nextNode = null;
            return obj.AtLocation;
        }
        obj.ChooseInitalStarterDirection();
        obj.FindTargetNode();
        
        //Exit to move Again
        return obj.MovingToLocation;
    }
}
