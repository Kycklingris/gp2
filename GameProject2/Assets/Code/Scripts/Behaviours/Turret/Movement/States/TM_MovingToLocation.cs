using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Unity.VisualScripting;
using UnityEngine;

public class TM_MovingToLocation : ITurretMoveState
{

    public virtual ITurretMoveState DoState(TurretMoveFSM obj)
    {
        obj.stateIndicator = TurretMoveFSMStates.MovingToLocation;
        //exit if we are at correct node
        if (obj.currentNode == obj.targetNode)
        {
            obj.direction = TurretMoveDirection.none;
            obj.nextNode = null;
            return obj.AtLocation;
        }

        
        // correct Search path
        obj.FindNextNode();

        
        // Instant movement
        
        // obj.transform.position = obj.nextNode.transform.position;
        //
        // obj.currentNode = obj.nextNode;

        // Slow Movement
        switch (obj.Moving)
        {
            //if we are Not moving find place to move to
            case false:
                obj.Moving = true;
                obj.FindNextNode();
                
                break;
            
            //move to current next
        
            case true:
                
                obj.transform.DOMove(obj.currentNode.transform.position, obj.moveDuration);
                
                var distanceToNext =
                    Vector3.Distance(obj.currentNode.transform.position, obj.transform.transform.position);
                
                if (distanceToNext < 0.1f)
                {
                    obj.Moving = false;
                    obj.currentNode = obj.nextNode;
                }
                
                break;
        
        }
        
        //Exit to move Again
        return obj.MovingToLocation;
    }





    // public virtual ITurretMoveState DoState(TurretMoveFSM obj)
    // {
    //     
    //     
    //     
    //     //exit if we are at correct node
    //     if (obj.currentNode == obj.targetNode)
    //     {
    //         obj.direction = TurretMoveDirection.none;
    //         obj.nextNode = null;
    //         return obj.AtLocation;
    //     }
    //     // State
    //
    //     
    //     // Move State
    //     // switch (obj.Moving)
    //     // {
    //     //     //if we are Not moving find place to move to
    //     //     case false:
    //     //         obj.Moving = true;
    //     //         obj.FindNextNode();
    //     //         
    //     //         break;
    //     //     
    //     //     //move to current next
    //     //
    //     //     case true:
    //     //         
    //     //         obj.transform.DOMove(obj.currentNode.transform.position, obj.moveDuration);
    //     //         
    //     //         var distanceToNext =
    //     //             Vector3.Distance(obj.currentNode.transform.position, obj.transform.transform.position);
    //     //         
    //     //         if (distanceToNext < 0.1f)
    //     //         {
    //     //             obj.Moving = false;
    //     //             obj.currentNode = obj.nextNode;
    //     //         }
    //     //         
    //     //         break;
    //     //
    //     // }
    //     
    //     
    //     // switch (obj.direction)
    //     // {
    //     //     case TurretMoveDirection.Left:
    //     //         obj.Left();
    //     //         break;
    //     //     case TurretMoveDirection.Right:
    //     //         obj.Right();
    //     //         break;
    //     // }
    //     
    //     // State Exit
    //     return obj.MovingToLocation;
    //
    // }
}
