using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Mirror;
using UnityEngine;

public class TurretMoveFSM : NetworkBehaviour
{
    private ITurretMoveState state;
    public TurretMoveFSMStates stateIndicator = TurretMoveFSMStates.none;
    
    
    //States
    public TM_AtLocation AtLocation = new TM_AtLocation();
    public TM_MovingToLocation MovingToLocation = new TM_MovingToLocation();
    public TM_SearchTarget SearchTarget = new TM_SearchTarget();

    [Header("Movement")]
    [SyncVar]
    public TurretSector targetSector;
    public float moveDuration; 
    
    [Header("Movement Debug")]
    public TurretMoveDirection direction;
    public TurretMoveDirection targetDirection;
    public TurretNode currentNode;
    public TurretNode nextNode;
    public TurretNode targetNode;
    public bool Moving;
    public bool HasReachedTarget;
    
    
    public BossMoveFSM bossMoveFsm;
    public List<BossNode> BossNodes = new List<BossNode>();
    
    
    [Space] 
    [Header("Decision making")] 
    public float DecisionTimer = 0.5f;


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        if (targetNode)
            Gizmos.DrawRay(targetNode.transform.position, Vector3.up);

        
        Gizmos.color = Color.green;

        if (nextNode)
            Gizmos.DrawRay(targetNode.transform.position, Vector3.up);
        
        Gizmos.color = Color.red;

        if (currentNode)
            Gizmos.DrawRay(currentNode.transform.position, Vector3.up);

        
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, Vector3.up);
        
        
        
        
    }


    private void Start()
    {
        if (!isServer)
            return;

        state = AtLocation;
        transform.position = currentNode.transform.position;
        
        StartCoroutine(MakeDecicison());
    }


    private void FixedUpdate()
    {
        if (!isServer)
            return;
        
        SetTargetZone();

        //if (HasReachedTarget)
    }

    private IEnumerator MakeDecicison()
    {
        while (true)
        {
            state = state.DoState(this);
            yield return new WaitForSeconds(DecisionTimer);
        }
    }

    public void FindNextNode()
    {
        switch (direction)
        {
            case TurretMoveDirection.none:
                break;
            case TurretMoveDirection.Left:
                nextNode = currentNode.Left;
                break;
            case TurretMoveDirection.Right:
                nextNode =  currentNode.Right;
                break;
        }
    }

    public void SetTargetZone()
    {
        if (!isServer)
            return;
        
        
        if (bossMoveFsm.CurrentNode == BossNodes[0])
            targetSector = TurretSector.Green;
        
        if (bossMoveFsm.CurrentNode == BossNodes[1])
            targetSector = TurretSector.Yellow;
        
        if (bossMoveFsm.CurrentNode == BossNodes[2])
            targetSector = TurretSector.Red;
        
        if (bossMoveFsm.CurrentNode == BossNodes[3])
            targetSector = TurretSector.Blue;
        
    }
    
    
    private void SetTargetDirection()
    {
        if (!isServer)
            return;
        
        switch (direction)
        {
            case TurretMoveDirection.none:
                break;
            case TurretMoveDirection.Left:
                targetDirection = TurretMoveDirection.Right;
                break;
            case TurretMoveDirection.Right:
                targetDirection = TurretMoveDirection.Left;
                break;
        }
    }

    private bool TargetCheck(TurretNode node)
    {
        // This might stop on a node marked none!
        if (node.Direction != direction && node.Sector == targetSector)
            return true;
        
        return false;
    }
    
    public void FindTargetNode()
    {
        
        //check if current fits the 
        TurretNode searchNode = currentNode;
        TurretMoveDirection searchDirection = currentNode.Direction;
        
        while (true)
        {
            if (TargetCheck(searchNode))
            {
                targetNode = searchNode;
                return;
            }
            switch (searchDirection)
            {
                case TurretMoveDirection.Left:
                    searchNode = searchNode.Left;
                    break;
                case TurretMoveDirection.Right:
                    searchNode = searchNode.Right;
                    break;
            }
        }
    }
    
    public void ChooseInitalStarterDirection()
    {
        //Debug.Log("");
        direction = currentNode.Direction;
        SetTargetDirection();
    }
    
    public void Right()
    {
        nextNode = nextNode.Right;
        currentNode = currentNode.Right;
    }

    public void Left()
    {
        nextNode = nextNode.Left;
        currentNode = currentNode.Left;
    }
}
