using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretTrajectory : MonoBehaviour
{

    //[SerializeField]
    //private Projectile projectile;

    //[SerializeField]
    //private Transform target;

    //[SerializeField]
    //private Transform projectileSpawn;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float maxHeight;

    [SerializeField]
    private float projectileMass;


    private void Start()
    {
        speed = 10;
        maxHeight = 20;
        projectileMass = 5;
    }

    public static bool ProjectileArc(Vector3 projectilePos, float lateralSpeed, Vector3 targetPos, float maxHeight, out Vector3 fireVelocity, out float gravity)
    {

        fireVelocity = Vector3.zero;
        gravity = float.NaN;

        Vector3 diff = targetPos - projectilePos;
        Vector3 diffXZ = new Vector3(diff.x, 0f, diff.z);
        float lateralDist = diffXZ.magnitude;

        if (lateralDist == 0)
            return false;

        float time = lateralDist / lateralSpeed;

        fireVelocity = diffXZ.normalized * lateralSpeed;


        float a = projectilePos.y;
        float b = maxHeight;
        float c = targetPos.y;

        gravity = -4 * (a - 2 * b + c) / (time * time);
        fireVelocity.y = -(3 * a - 4 * b + c) / time;

        return true;
    }

    /*
    public static int projectile_arc(Vector3 proj_pos, float proj_speed, Vector3 target, float gravity, out Vector3 s0, out Vector3 s1)
    {

        s0 = Vector3.zero;
        s1 = Vector3.zero;

        Vector3 diff = target - proj_pos;
        Vector3 diffXZ = new Vector3(diff.x, 0f, diff.z);
        float groundDist = diffXZ.magnitude;

        float speed2 = proj_speed * proj_speed;
        float speed4 = proj_speed * proj_speed * proj_speed * proj_speed;
        float y = diff.y;
        float x = groundDist;
        float gx = gravity * x;

        float root = speed4 - gravity * (gravity * x * x + 2 * y * speed2);

        if (root < 0)
            return 0;

        root = Mathf.Sqrt(root);

        float lowAng = Mathf.Atan2(speed2 - root, gx);
        float highAng = Mathf.Atan2(speed2 + root, gx);
        int numSolutions = lowAng != highAng ? 2 : 1;

        Vector3 groundDir = diffXZ.normalized;
        s0 = groundDir * Mathf.Cos(lowAng) * proj_speed + Vector3.up * Mathf.Sin(lowAng) * proj_speed;
        if (numSolutions > 1)
            s1 = groundDir * Mathf.Cos(highAng) * proj_speed + Vector3.up * Mathf.Sin(highAng) * proj_speed;

        return numSolutions;
    }*/

    [ContextMenu("Shoot")]
    public void Shoot(Projectile projectile, Transform target, Transform projectileSpawn, out GameObject newBullet)
    {
        transform.LookAt(target);

        newBullet = Instantiate(projectile.Prefab, projectileSpawn.position, Quaternion.LookRotation(target.position));

        var rb = newBullet.GetComponent<Rigidbody>();

        rb.useGravity = false;
        rb.mass = projectileMass;

        newBullet.AddComponent<ConstantForce>();
        
        ProjectileArc(newBullet.transform.position, speed, target.position, maxHeight, out Vector3 fireVelocity, out float gravity);
        Debug.Log(Vector3.Distance(newBullet.transform.position, target.position));

        newBullet.GetComponent<ConstantForce>().force = new Vector3(0, -gravity*rb.mass, 0);

        rb.AddForce(fireVelocity*rb.mass, ForceMode.Impulse);

        //rb.AddForce(projectileSpawn.forward * projectile.MuzzleSpeed, ForceMode.Impulse);

        /*
        Vector3 s0;
        Vector3 s1;
        Debug.Log(projectile_arc(newBullet.transform.position, 20, target.position, -Physics.gravity.y, out s0, out s1));
        Debug.Log(Physics.gravity);
        Debug.Log(s0);
        Debug.Log(s1);
        
        rb.AddForce(s1.normalized, ForceMode.Impulse);*/

    }

}
