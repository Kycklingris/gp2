%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: SupportUpperMask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: BezierCurve.005
    m_Weight: 1
  - m_Path: Cube
    m_Weight: 1
  - m_Path: Cube.001
    m_Weight: 1
  - m_Path: Cube.002
    m_Weight: 0
  - m_Path: Cube.003
    m_Weight: 0
  - m_Path: Cube.004
    m_Weight: 1
  - m_Path: Cube.005
    m_Weight: 1
  - m_Path: Cube.006
    m_Weight: 1
  - m_Path: Cube.007
    m_Weight: 1
  - m_Path: Cube.008
    m_Weight: 1
  - m_Path: Cube.010
    m_Weight: 1
  - m_Path: Cube.013
    m_Weight: 1
  - m_Path: Cube.015
    m_Weight: 1
  - m_Path: Cube.018
    m_Weight: 1
  - m_Path: Cube.019
    m_Weight: 1
  - m_Path: Cube.031
    m_Weight: 1
  - m_Path: Cylinder
    m_Weight: 1
  - m_Path: Cylinder.001
    m_Weight: 1
  - m_Path: Cylinder.002
    m_Weight: 0
  - m_Path: Cylinder.003
    m_Weight: 0
  - m_Path: Cylinder.004
    m_Weight: 1
  - m_Path: Cylinder.006
    m_Weight: 1
  - m_Path: Cylinder.007
    m_Weight: 1
  - m_Path: Cylinder.015
    m_Weight: 1
  - m_Path: Cylinder.018
    m_Weight: 0
  - m_Path: Plane.001
    m_Weight: 1
  - m_Path: Robot
    m_Weight: 0
  - m_Path: Robot/FootIK.L
    m_Weight: 0
  - m_Path: Robot/FootIK.L/FootIK.L_end
    m_Weight: 0
  - m_Path: Robot/FootIK.R
    m_Weight: 0
  - m_Path: Robot/FootIK.R/FootIK.R_end
    m_Weight: 0
  - m_Path: Robot/HandIK.L
    m_Weight: 1
  - m_Path: Robot/HandIK.L/HandIK.L_end
    m_Weight: 1
  - m_Path: Robot/HandIK.R
    m_Weight: 1
  - m_Path: Robot/HandIK.R/HandIK.R_end
    m_Weight: 1
  - m_Path: Robot/Root
    m_Weight: 0
  - m_Path: Robot/Root/Hip
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Leg.L
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Leg.L/Shin.L
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Leg.L/Shin.L/Foot.L
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Leg.L/Shin.L/Foot.L/Foot.L_end
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Leg.R
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Leg.R/Shin.R
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Leg.R/Shin.R/Foot.R
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Leg.R/Shin.R/Foot.R/Foot.R_end
    m_Weight: 0
  - m_Path: Robot/Root/Hip/Spine 1
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Neck
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Neck/Head
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Index1.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Index1.L/Index2.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Index1.L/Index2.L/Index3.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Index1.L/Index2.L/Index3.L/Index3.L_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Middle1.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Middle1.L/Middle2.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Middle1.L/Middle2.L/Middle3.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Middle1.L/Middle2.L/Middle3.L/Middle3.L_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Pinky1.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Pinky1.L/Pinky2.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Pinky1.L/Pinky2.L/Pinky3.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Pinky1.L/Pinky2.L/Pinky3.L/Pinky3.L_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Ring1.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Ring1.L/Ring2.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Ring1.L/Ring2.L/Ring3.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Ring1.L/Ring2.L/Ring3.L/Ring3.L_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Thumb1.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Thumb1.L/Thumb2.L
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.L/Arm.L/ForeArm.L/Hand.L/Thumb1.L/Thumb2.L/Thumb2.L_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hack Stick
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hack Stick/Hack
      Stick_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Index1.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Index1.R/Index2.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Index1.R/Index2.R/Index3.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Index1.R/Index2.R/Index3.R/Index3.R_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Middle1.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Middle1.R/Middle2.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Middle1.R/Middle2.R/Middle3.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Middle1.R/Middle2.R/Middle3.R/Middle3.R_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Pinky1.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Pinky1.R/Pinky2.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Pinky1.R/Pinky2.R/Pinky3.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Pinky1.R/Pinky2.R/Pinky3.R/Pinky3.R_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Ring1.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Ring1.R/Ring2.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Ring1.R/Ring2.R/Ring3.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Ring1.R/Ring2.R/Ring3.R/Ring3.R_end
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Thumb1.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Thumb1.R/Thumb2.R
    m_Weight: 1
  - m_Path: Robot/Root/Hip/Spine 1/Spine 2/Shoulder.R/Arm.R/ForeArm.R/Hand.R/Thumb1.R/Thumb2.R/Thumb2.R_end
    m_Weight: 1
  - m_Path: Sphere.001
    m_Weight: 0
  - m_Path: Sphere.002
    m_Weight: 0
  - m_Path: Sphere.003
    m_Weight: 1
  - m_Path: Vert
    m_Weight: 0
